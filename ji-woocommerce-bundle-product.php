<?php

/**
 * @since           1.0.0
 * @package         ji-woocommerce-bundle-product
 * 
 * Plugin Name:     Ji Woocommerce Bundle Product
 * Plugin URI:
 * Description:     Buy bundle product in WooCommerce
 * Author:          Duy Lee
 * Version:         1.0.0
 * Author URI:
 * Text Domain:     JWBP
 * Domain Path:     /languages
 */

/**
 * Making sure woocommerce is there 
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if(!is_plugin_active( 'woocommerce/woocommerce.php')){
    function jwbp_not_woocommerce_active() {
        ?>
        <div class="error notice">
            <p><?php _e( 'Please Install and Activate WooCommerce plugin, without that this plugin cant work', 'JWBP' ); ?></p>
        </div>
        <?php
    }
    add_action( 'admin_notices', 'jwbp_not_woocommerce_active' );
    deactivate_plugins(plugin_basename(__FILE__));
    return;
}


/**
 * Currently plugin version.
 * Rename this for your plugin and update it as you release new versions.
 */
define('JWBP_TEXT_DOMAIN', 'JWBP');
define('JWBP_VERSION', '1.0.0');
define('JWBP_PLUGIN_URL', plugin_dir_url(__FILE__));
define('JWBP_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('JWBP_PLUGIN_BASENAME', plugin_basename(__FILE__));
define('JWBP_NAME', 'ji-woocommerce-bundle-product');
define('JWBP_POST_TYPE', 'ji-woo-bundle');
define('JWBP_PAGE_LINK', 'ji-woocommerce-bundle-product');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ji-woocommerce-bundle-product-activator.php
 */
function activate_ji_woocommerce_bundle_product() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ji-woocommerce-bundle-product-activator.php';
	Ji_Woocommerce_Bundle_Product_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ji-woocommerce-bundle-product-deactivator.php
 */
function deactivate_ji_woocommerce_bundle_product() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ji-woocommerce-bundle-product-deactivator.php';
	Ji_Woocommerce_Bundle_Product_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ji_woocommerce_bundle_product' );
register_deactivation_hook( __FILE__, 'deactivate_ji_woocommerce_bundle_product' );


/**
 * Define the locale for this plugin for internationalization i18n
 */
 if (!function_exists('jwbp_load_plugin_textdomain')) {
    function jwbp_load_plugin_textdomain()
    {
        $plugin_rel_path = basename(dirname(__FILE__)) . '/languages';
        load_plugin_textdomain(JWBP_TEXT_DOMAIN, false, $plugin_rel_path);
    }
}
add_action('plugins_loaded', 'jwbp_load_plugin_textdomain');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ji-woocommerce-bundle-product.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.

 */
function run_ji_woocommerce_bundle_product() {

	$plugin = new Ji_Woocommerce_Bundle_Product();
	$plugin->run();

}
run_ji_woocommerce_bundle_product();
