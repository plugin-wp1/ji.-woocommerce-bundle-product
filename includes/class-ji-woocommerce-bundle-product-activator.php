<?php

/**
 * Fired during plugin activation
 *
 * @package    Ji_Woocommerce_Bundle_Product_Activator
 * @subpackage Ji_Woocommerce_Bundle_Product_Activator/includes
 */
class Ji_Woocommerce_Bundle_Product_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		add_option(JWBP_NAME, true);
	}

}