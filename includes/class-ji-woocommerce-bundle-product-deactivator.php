<?php

/**
 * Fired during plugin activation
 *
 * @package    Ji_Woocommerce_Bundle_Product_Deactivator
 * @subpackage Ji_Woocommerce_Bundle_Product_Deactivator/includes
 */
class Ji_Woocommerce_Bundle_Product_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
