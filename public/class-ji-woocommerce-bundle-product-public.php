<?php

/**
 * The public-facing functionality of the plugin.
 *
 */
class Ji_Woocommerce_Bundle_Product_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		add_action( 'plugins_loaded', array( __CLASS__, 'init_plugin' ), 10 );
	}

	static function init_plugin() {
		Jwbp_Cart::init();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ji_Woocommerce_Bundle_Product_Loader as all of the hooks are defined
		 * in that particular class.
         * 
		 */

		wp_enqueue_style( $this->plugin_name, JWBP_PLUGIN_URL . 'assets/public/css/ji-woocommerce-bundle-product-public.css', array(), time(), 'all' );
		// jquery modal
		wp_enqueue_style( 'jquery-modal', JWBP_PLUGIN_URL . 'assets/lib/jquery-modal/jquery.modal.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ji_Woocommerce_Bundle_Product_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 */

		wp_enqueue_script( $this->plugin_name, JWBP_PLUGIN_URL . 'assets/public/js/ji-woocommerce-bundle-product-public.js', array( 'jquery' ), time(), false );
		// jquery modal
		wp_enqueue_script( 'jquery-modal', JWBP_PLUGIN_URL . 'assets/lib/jquery-modal/jquery.modal.min.js', array( 'jquery' ), $this->version, false );

	}

}
