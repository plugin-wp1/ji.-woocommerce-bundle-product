
<button class="variant option bundle">
    <input type="radio" class="active-checkbox" name="jwbp_bundle" value="<?php echo $bundle->id ?>">

    <span class="image-wrap">
        <i class="image" style="background-image: url(<?php echo wp_get_attachment_image_src( $bundle->image )[0] ?>);"></i>
    </span>
    <span class="title-wrap">
        <span class="title">
            <span class="bundle-title"><?php echo $bundle->name ?></span>
            <span class="price">
                <s></s>
                <span class="value">
                    <span class="money" data-original-value="<?php echo $bundle->price ?>"><?php echo strip_tags(wc_price($bundle->price)) ?></span>
                </span>
            </span>
        </span>
        <span class="bundle-description">
            <span class="desc b1"><?php echo $bundle->short_description ?></span>
            <span class="desc-btn b1">
                <a href="#modal-info-<?php echo $key ?>" rel="modal:open"><?php _e('view info', JWBP_TEXT_DOMAIN) ?></a>

                <!-- modal -->
                <div style="display: none;">
                    <div id="modal-info-<?php echo $key ?>" class="modal view-info-modal">
                        <h3><?php echo $bundle->name ?></h3>
                        <?php echo htmlspecialchars($bundle->view_info) ?>
                    </div>
                </div>
                
            </span>
        </span>
    </span>
</button>