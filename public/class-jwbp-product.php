<?php

class Jwbp_Product {
    function __construct() {
        $global_disable = get_option(JWBP_NAME, 0);

        if($global_disable){
            add_action( 'woocommerce_after_add_to_cart_quantity', array($this,'jwbp_productBundleInit'), 5);
            add_action( 'wp_enqueue_scripts', array($this,'style') );
        }
    }

    function jwbp_productBundleInit() {
        global $product;
        $product_id = $product->get_ID();

        // get jwbp bundle data
        $jwbp_data = get_post_meta($product_id, 'jwbp_bundle', true);

        if($jwbp_data['status'] != 'yes' && empty($jwbp_data['ids'])) {
            return;
        }

        $this->jwbp_productLoadBundle($product_id, $jwbp_data['ids']);
    }

    function jwbp_productLoadBundle($product_id, $jwbp_ids) {

        $jwbp_ids = explode(',', $jwbp_ids);
    ?>

        <div id="jwbp_bundle" class="option-block-wrap">
            <div class="bundle-header"></div>

            <div class="option-block bundle-block save-with-an-accessories-bundle">
                <div class="variants only-one">

                    <?php
                    foreach ($jwbp_ids as $key => $jwbp_id) {
                        $bundle = new Jwbp_Model($jwbp_id);

                        include 'partials/jwbp-product-bundle-option.php';
                    }
                    ?>

                </div>
            </div>
        </div>

    <?php
    }
}

new Jwbp_Product();