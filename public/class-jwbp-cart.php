<?php

class Jwbp_Cart
{

    public static function init()
    {
        $global_disable = get_option(JWBP_NAME, 0);

        if ($global_disable) {
            add_filter('woocommerce_add_cart_item_data', array(__CLASS__, 'jwbp_addBundleProductToCartItem'), 10, 3);
            add_filter('woocommerce_get_item_data', array(__CLASS__, 'jwbp_displayBundleProductInCart'), 10, 2);

            add_action('woocommerce_add_to_cart', array(__CLASS__, 'jwbp_addProductToCart'));
            add_action('woocommerce_cart_item_restored', array(__CLASS__, 'jwbp_addProductToCart'));

            add_filter( 'woocommerce_get_cart_item_from_session', array( __CLASS__, 'jwbp_validateChildItemInCart' ), 100, 3 );

            add_action( 'woocommerce_remove_cart_item', array( __CLASS__, 'jwbp_removeProductInCart' ) );

            add_filter( 'woocommerce_cart_item_remove_link', array( __CLASS__, 'jwbp_removeLink' ), 10, 2 );
            add_filter( 'woocommerce_cart_item_quantity', array( __CLASS__, 'jwbp_removeQuantityEdit' ), 10, 3 );
        }
    }

    // Add jwbp_bundle text to cart item.
    public static function jwbp_addBundleProductToCartItem($cart_item_data, $product_id, $variation_id)
    {
        $jwbp_bundle = filter_input(INPUT_POST, 'jwbp_bundle');

        if (empty($jwbp_bundle)) {
            return $cart_item_data;
        }

        $cart_item_data['jwbp_bundle'] = $jwbp_bundle;

        return $cart_item_data;
    }

    public static function jwbp_addProductToCart($cart_item_key)
    {
        $parent = new Jwbp_Cart_Parent($cart_item_key);
        remove_action('woocommerce_add_to_cart', array(__CLASS__, 'jwbp_addProductToCart'));
        $parent->addChildProduct();
        add_action('woocommerce_add_to_cart', array(__CLASS__, 'jwbp_addProductToCart'));
    }

    public static function jwbp_validateChildItemInCart($session_data, $values, $key)
    {
        if (Jwbp_Bundle_Product::isChildProduct($session_data)) {
            Jwbp_Bundle_Product::setPriceBundle($session_data['data'], $values);
        }
        return $session_data;
    }

    public static function jwbp_removeProductInCart($cart_item_key)
    {
        if (isset($_GET['remove_item'])) {
            $parent = new Jwbp_Cart_Parent($cart_item_key);
            $parent->removeChildProduct();
        }
    }

    public static function jwbp_removeQuantityEdit($product_quantity, $cart_item_key, $cart_item)
    {
        if (Jwbp_Bundle_Product::isChildProduct($cart_item_key)) {
            $product_quantity = sprintf('%s <input type="hidden" name="cart[%s][qty]" value="%s" />', $cart_item['quantity'], $cart_item_key, $cart_item['quantity']);
        }
        return $product_quantity;
    }

    public static function jwbp_removeLink($remove_link, $cart_item_key)
    {
        if (Jwbp_Bundle_Product::isChildProduct($cart_item_key)) {
            $remove_link = '';
        }
        return $remove_link;
    }
    
}
