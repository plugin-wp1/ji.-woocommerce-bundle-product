<?php

class Jwbp_Rule
{

    protected $cart_item_key;
    public $rule = array(
        'enable' => 'no',
        'product_ids' => array(),
    );

    function __construct($cart_item_key)
    {
        $this->cart_item_key = $cart_item_key;
        $this->parent_product_id = $this->getProductID();
        $this->bundle_id = $this->getBundleID();

    }

    function rule()
    {
        if($this->bundle_id != 0) {
            $jwbp_bundle = new Jwbp_Model($this->bundle_id);
            if(!empty($jwbp_bundle)) {
                $this->rule = array(
                    'enable' => $jwbp_bundle->getStatus(),
                    'product_ids' => $jwbp_bundle->getProductIDs(),
                );
            }
        }
        return $this->rule;
    }

    function getProductID()
    {
        $product_id = 0;
        if (isset(WC()->cart->cart_contents[$this->cart_item_key]['product_id'])) {
            $product_id = WC()->cart->cart_contents[$this->cart_item_key]['product_id'];
        }
        return $product_id;
    }

    function getBundleID()
    {
        $bundle_id = 0;
        if (isset(WC()->cart->cart_contents[$this->cart_item_key]['jwbp_bundle'])) {
            $bundle_id = WC()->cart->cart_contents[$this->cart_item_key]['jwbp_bundle'];
        }
        return $bundle_id;
    }
}
