<?php

class Jwbp_Model {

    private $bundle_array;
    // public data
    public $status = 'no';
    public $id = null;
    public $name = null;
    public $image = null;
    public $product_ids = null;
    public $price = null;
    public $short_description = null;
    public $view_info = null;

    public function __construct($jwbp_id) {

        $bundle = get_post_meta($jwbp_id, JWBP_POST_TYPE, true);
        $bundle = json_decode($bundle, true);


        if(!empty($bundle)) {
            
            // set data
            $this->bundle_array = $bundle;

            $this->status = 'yes';
            $this->id = $jwbp_id;
            $this->name = $bundle['bundle_name'];
            $this->image = $bundle['bundle_image'];
            $this->product_ids = explode(',', $bundle['bundle_products']);
            $this->price = $bundle['bundle_sale_price'];
            $this->short_description = $bundle['bundle_short_description'];
            $this->view_info = $bundle['bundle_view_info'];
        
            return $this;
        } else {
            return null;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getProductIDs()
    {
        return $this->product_ids;
    }

    function getPrice()
    {
        return $this->price;
    }

}
