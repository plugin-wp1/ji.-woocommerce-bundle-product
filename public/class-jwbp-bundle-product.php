<?php

class Jwbp_Bundle_Product
{

    function __construct()
    {
    }

    public static function addChildProduct($parent_product_id, $parent_key)
    {
        $child_product_key = WC()->cart->add_to_cart(
            $parent_product_id,
            1,
            0,
            array(),
            array('parent_key' => $parent_key)
        );
        return $child_product_key;
    }

    public static function removeChildProduct($child_product_key)
    {
        if (isset(WC()->cart->cart_contents[$child_product_key])) {
            WC()->cart->remove_cart_item($child_product_key);
        }
    }

    public static function updateQuantity($free_product_key, $quantity, $parent_item_key = false)
    {
        if (isset(WC()->cart->cart_contents[$free_product_key])) {
            WC()->cart->cart_contents[$free_product_key]['quantity'] = $quantity;
        } else {
            if ($parent_item_key) {
                Pi_Bogo_Cart::addProductToCart($parent_item_key);
            }
        }
    }

    public static function isChildProduct($cart_item_key)
    {
        if (is_string($cart_item_key)) {
            if (isset(WC()->cart->cart_contents[$cart_item_key]['parent_key'])) {
                return true;
            }
        } else {
            /**
             * If cart_item_key is object and not the key
             */
            if (isset($cart_item_key['parent_key'])) {
                return true;
            }
        }
        return false;
    }

    public static function setPriceBundle(&$product, $cart_item_value)
    {
        $bundle_id = $cart_item_value['jwbp_bundle'];
        $bundl_obj = new Jwbp_Model($bundle_id);
        $bundl_price = $bundl_obj->getPrice();
        if($bundl_obj) {
            $product->set_name($bundl_obj->getName());
            $product->set_image_id($bundl_obj->getImage());
            $product->image_id = $bundl_obj->getImage();
            $product->set_price($bundl_price);
            $product->set_sale_price($bundl_price);
            $product->set_regular_price($bundl_price);
            $product->is_child_product = true;
        }
    }

    public static function getParent($free_product_key)
    {
        if (isset(WC()->cart->cart_contents[$free_product_key])) {
            return WC()->cart->cart_contents[$free_product_key]['parent_key'];
        }
    }
}
