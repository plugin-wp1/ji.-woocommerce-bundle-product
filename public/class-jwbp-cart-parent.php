<?php

class Jwbp_Cart_Parent
{

    protected $cart_item_key;

    function __construct($cart_item_key)
    {
        $this->cart_item_key = $cart_item_key;

        $rule_obj = new Jwbp_Rule($this->cart_item_key);
        $this->rule = $rule_obj->rule();
    }

    function addChildProduct()
    {
        if ($this->rule['enable'] == 'yes') {

            $parent_product_id = $this->getProductID();
            $bundle_id = $this->getBundleID();

            $child_product_key = Jwbp_Bundle_Product::addChildProduct($parent_product_id, $bundle_id, $this->cart_item_key);

            $this->addChildProductKey($child_product_key, $bundle_id);
        }
    }

    function removeChildProduct()
    {
        if ($this->isParent()) {
            $child_key = $this->getChildKey();
            Jwbp_Bundle_Product::removeChildProduct($child_key);
        }
    }

    function addChildProductKey($child_key, $bundle_id)
    {
        if (isset(WC()->cart->cart_contents[$this->cart_item_key])) {
            WC()->cart->cart_contents[$this->cart_item_key]['child_key'] = $child_key;
            WC()->cart->cart_contents[$this->cart_item_key]['jwbp_bundle'] = $bundle_id;
        }
    }

    function isParent()
    {
        $child_key = $this->getChildKey();
        if ($child_key) {
            return true;
        }
        return false;
    }

    // get product cart >>>

    function getProductID()
    {
        $product_id = 0;
        if (isset(WC()->cart->cart_contents[$this->cart_item_key]['product_id'])) {
            $product_id = WC()->cart->cart_contents[$this->cart_item_key]['product_id'];
        }
        return $product_id;
    }

    function getVariationID()
    {
        $variation_id = 0;
        if (isset(WC()->cart->cart_contents[$this->cart_item_key]['variation_id'])) {
            $variation_id = WC()->cart->cart_contents[$this->cart_item_key]['variation_id'];
        }
        return $variation_id;
    }

    function getQuantity()
    {
        $quantity = 0;
        if (isset(WC()->cart->cart_contents[$this->cart_item_key]['quantity'])) {
            $quantity = WC()->cart->cart_contents[$this->cart_item_key]['quantity'];
        }
        return $quantity;
    }

    function getChildKey()
    {
        $child_key = false;
        if (isset(WC()->cart->cart_contents[$this->cart_item_key]['child_key'])) {
            $child_key = WC()->cart->cart_contents[$this->cart_item_key]['child_key'];
        }
        return $child_key;
    }

    function getBundleID()
    {
        $bundle_id = 0;
        if (isset(WC()->cart->cart_contents[$this->cart_item_key]['jwbp_bundle'])) {
            $bundle_id = WC()->cart->cart_contents[$this->cart_item_key]['jwbp_bundle'];
        }
        return $bundle_id;
    }
}
