
jQuery(document).ready( function($) {

    $('#jwbp_bundle').on('click', 'button.option', function(e) {
        e.preventDefault();
        
        $('#jwbp_bundle button.option').removeClass('is-active');
        $(this).addClass('is-active');
        $(this).find('.active-checkbox').prop("checked", true);

    });

});