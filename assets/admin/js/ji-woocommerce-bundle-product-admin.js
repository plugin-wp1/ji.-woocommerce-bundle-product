jQuery(document).ready( function($) {
    var custom_uploader;

    // event select image media
    $('.jwbp_btn_add_img').click(function(e) {
       e.preventDefault();
        parent = $(this).parents('.bundle_image_field');

        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: true
        });

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            parent.find('.jwbp_input_img').val(attachment.id);
            parent.find('img').attr('srcset', '').attr('src', attachment.url);
        });

        //Open the uploader dialog
        custom_uploader.open(); 
    });

    // Search product select2
    $('.jwbp-product-search').select2({
        minimumInputLength: 3,
        ajax: {
            type: "GET",
            url: '/wp-admin/admin-ajax.php',      
            dataType: 'json',
            delay: 1000,
            data: function (term) {
                return {
                    action: "jwbp_search_products",
                    keyword: term.term,
                };
            },
            processResults: function (response) {
                return {
                   results: response
                };
            },
            cache: true
        }

    });

});