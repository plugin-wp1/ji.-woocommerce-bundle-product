<?php

class Ji_Woocommerce_Bundle_Product_Admin_Menu {

    public $plugin_name;
    public $menu;
    
    function __construct($plugin_name , $version) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
        
        add_action( 'admin_menu', array($this,'jwbp_plugin_menu') );
        add_action('init', array($this, 'jwbp_create_post_type_bundle_product'));
        add_action('save_post', array($this, 'jwbp_save_post_type_bundle_product'));
        add_action('admin_init', array($this, 'jwbp_add_main_form_meta_boxes'));
        add_action( 'wp_ajax_jwbp_search_products', array( $this, 'jwbp_search_products' ) );
    }

    function jwbp_plugin_menu() {
        
        $this->menu = add_menu_page(
            __( 'Ji. Buy bundle product', JWBP_TEXT_DOMAIN),
            __( 'Ji. Buy bundle product', JWBP_TEXT_DOMAIN),
            'manage_options',
            JWBP_NAME,
            null,
            'dashicons-heart',
            20
        );
    }

    // Register post type
    public function jwbp_create_post_type_bundle_product()
    {
        $args = array(
            'labels' => array(
                'name' => 'Ji. Buy bundle product',
                'singular_name' => 'Ji. Buy bundle product',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Bundle Product',
                'edit_item' => 'Edit Bundle Product',
                'new_item' => 'New Bundle Product',
                'view_item' => 'View Bundle Product',
                'search_items' => 'Search Bundle Product',
                'not_found' => 'Nothing Found',
                'not_found_in_trash' => 'Nothing found in the Trash',
                'parent_item_colon' => ''
            ),
            'show_in_menu' => JWBP_NAME,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 0,
            'supports' => array('title')
        );

        register_post_type(JWBP_POST_TYPE, $args);

        add_action("admin_init", array($this, "jwbp_admin_style"));
    }

    // Save post type data
    public function jwbp_save_post_type_bundle_product($post_id)
    {
        global $post;

        if (!$post || $post->post_type != JWBP_POST_TYPE || $post_id != $post->ID) {
            return;
        }

        // get data
        $requests = [
            'bundle_name' => $_POST['bundle_name'], 
            'bundle_image' => $_POST['bundle_image'], 
            'bundle_products' => $_POST['bundle_products'], 
            'bundle_view_info' => $_POST['bundle_view_info'], 
            'bundle_short_description' => $_POST['bundle_short_description'], 
            'bundle_sale_price' => $_POST['bundle_sale_price'],
        ];

        if ($requests) {
            update_post_meta($post->ID, JWBP_POST_TYPE, json_encode($requests, JSON_UNESCAPED_UNICODE));
        }
    }

    // Register style, script
    public function jwbp_admin_style() {
        wp_register_script( 'selectWoo', WC()->plugin_url() . '/assets/js/selectWoo/selectWoo.full.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'selectWoo' );
        wp_enqueue_style( 'admin', WC()->plugin_url() . '/assets/css/admin.css');
        wp_enqueue_style( 'select2', WC()->plugin_url() . '/assets/css/select2.css');

        wp_enqueue_script( $this->plugin_name, JWBP_PLUGIN_URL . 'assets/admin/js/ji-woocommerce-bundle-product-admin.js', array( 'jquery', 'selectWoo' ), time(), false );
	}

    // function add meta box form bundle selection
    public function jwbp_add_main_form_meta_boxes()
    {
        add_meta_box(
            "jwbp_bundle_product_meta",
            __('Bundle Product Form', JWBP_TEXT_DOMAIN),
            array($this, "jwbp_add_bundle_product_meta_box"),
            JWBP_POST_TYPE,
            "normal",
            "low"
        );
    }
    public function jwbp_add_bundle_product_meta_box()
    {
        global $post;

        // get data
        $bundle_data = get_post_meta($post->ID, JWBP_POST_TYPE, true);
        $bundle_data = json_decode($bundle_data, true);
        // get fields
        $bundle_name = isset($bundle_data['bundle_name']) ? $bundle_data['bundle_name'] : null;
        $bundle_image = isset($bundle_data['bundle_image']) ? $bundle_data['bundle_image'] : null;
        $bundle_products = isset($bundle_data['bundle_products']) ? $bundle_data['bundle_products'] : null;
        $bundle_view_info = isset($bundle_data['bundle_view_info']) ? $bundle_data['bundle_view_info'] : null;
        $bundle_short_description = isset($bundle_data['bundle_short_description']) ? $bundle_data['bundle_short_description'] : null;
        $bundle_sale_price = isset($bundle_data['bundle_sale_price']) ? $bundle_data['bundle_sale_price'] : null;

        // include meta form bundle
        include 'partials/jwbp-metabox-bundle.php';
    }

    // ajax search product select2
    public function jwbp_search_products( $x = '', $post_types = array( 'product' ) ) {

		if ( ! current_user_can( 'manage_options' ) && $_GET['action'] == 'jwbp_search_products' ) {
			return;
		}

        ob_start();
        
        if(!isset($_GET['keyword'])) die;

		$keyword = isset($_GET['keyword']) ? sanitize_text_field($_GET['keyword']) : "";

		if ( empty( $keyword ) ) {
			die();
		}
		$arg = array(
			'post_status'    => 'publish',
			'post_type'      => $post_types,
			'posts_per_page' => 50,
			's'              => $keyword

		);
		$the_query      = new WP_Query( $arg );
		$found_products = array();
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$prd = wc_get_product( get_the_ID() );
				$cat_ids  = wp_get_post_terms( get_the_ID(), 'product_cat', array( 'fields' => 'ids' ) );

				/* remove grouped product or external product */
				if($prd->is_type('grouped') || $prd->is_type('external')){
					continue;
				}
				

				if ( $prd->has_child() && $prd->is_type( 'variable' ) ) {
					
				} else {
					$product_id    = get_the_ID();
					$product_title = get_the_title();
					$the_product   = new WC_Product( $product_id );
					if ( ! $the_product->is_in_stock() ) {
						$product_title .= ' (Out of stock)';
					}
					$product          = array( 'id' => $product_id, 'text' => "{$product_id} : $product_title" );
					$found_products[] = $product;
				}
			}
        }
		wp_send_json( $found_products );
		die;
    }

}