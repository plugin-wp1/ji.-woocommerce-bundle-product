<?php

/**
 * The admin-specific functionality of the plugin.
 *
 */
class Ji_Woocommerce_Bundle_Product_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		new Ji_Woocommerce_Bundle_Product_Admin_Menu($this->plugin_name, $this->version);

		add_action( 'plugins_loaded', array( __CLASS__, 'init_plugin' ), 10 );

		// add_action('admin_init', array($this,'plugin_redirect'));
	}

	static function init_plugin() {
		Jwbp_Order::init();
	}

	// function plugin_redirect(){
	// 	if (get_option('pisol_bogo_redirect', false)) {
	// 		delete_option('pisol_bogo_redirect');
	// 		if(!isset($_GET['activate-multi']))
	// 		{
	// 			wp_redirect("admin.php?page=pisol-bogo-deal");
	// 		}
	// 	}
	// }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ji_Woocommerce_Bundle_Product_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 */

		wp_enqueue_style( $this->plugin_name, JWBP_PLUGIN_URL . 'assets/admin/css/ji-woocommerce-bundle-product-admin.css', array(), time(), 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ji_Woocommerce_Bundle_Product_Loader as all of the hooks are defined
		 * in that particular class.
         * 
		 */

		

	}

}
