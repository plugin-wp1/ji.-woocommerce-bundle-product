<?php

class Jwbp_Order
{

    public static function init()
    {
        $global_disable = get_option(JWBP_NAME, 0);

        if ($global_disable) {
            add_action('woocommerce_checkout_create_order_line_item', array(__CLASS__, 'jwbp_woocommerce_checkout_create_bundle_order_meta'), 10, 4);
            add_filter( 'woocommerce_admin_order_item_thumbnail', array(__CLASS__, 'jwbp_overrideBundleProductImage'), 10, 3);
        
        }
    }

    public static function jwbp_woocommerce_checkout_create_bundle_order_meta($item, $cart_item_key, $cart_item_value, $order)
    {
        if($cart_item_value['jwbp_bundle'] && Jwbp_Bundle_Product::isChildProduct($cart_item_key)) {
            $item->add_meta_data('jwbp_bundle', $cart_item_value['jwbp_bundle'], true);
        }
    }

    public static function jwbp_overrideBundleProductImage($image, $item_id, $item)
    {
        if($item['jwbp_bundle']) {
            var_dump($image);
            $bundle_id = $item['jwbp_bundle'];
            $bundle = new Jwbp_Model($bundle_id);
            if($bundle) {
                $image = wp_get_attachment_image($bundle->getImage());
            }
            
            return $image;
        }
    }
    
}
