
<div id="jwbp_meta_box" class="panel woocommerce_options_panel">
    <p class="form-field bundle_name_field">
        <label for="bundle_name"><?php _e('Bundle Name', JWBP_TEXT_DOMAIN) ?></label>
        <input type="text" class="short" style="" name="bundle_name" id="bundle_name" value="<?php echo $bundle_name ?>" placeholder="">
    </p>
    <p class="form-field bundle_image_field">
        <label><?php _e('Bundle Image', JWBP_TEXT_DOMAIN) ?></label>
        <input type="hidden" class="jwbp_input_img" name="bundle_image" value="<?php echo $bundle_name ?>'">
        
        <?php
        if ( $bundle_image ) {
            echo wp_get_attachment_image($bundle_image);
        } else {
            echo wp_get_attachment_image(get_option( 'woocommerce_placeholder_image', 0 ));
        }
        ?>

        <input type="button" class="button button-secondary jwbp_btn_add_img" value="<?php _e( 'Add Image', JWBP_TEXT_DOMAIN ) ?>"/>
    </p>
    <p class="form-field bundle_products_field">
        <label for="bundle_products"><?php esc_html_e( 'Grouped products', 'woocommerce' ); ?></label>
        <select class="jwbp-product-search" multiple="multiple" style="width: 50%;" id="bundle_products" name="bundle_products[]" data-sortable="true" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products">
            <?php

            foreach ( $bundle_products as $product_id ) {
                $product = wc_get_product( $product_id );
                if ( is_object( $product ) ) {
                    echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true, false ) . '>' . esc_attr( $product_id ) . ':' . htmlspecialchars( wp_kses_post( $product->get_formatted_name() ) ) . '</option>';
                }
            }
            ?>
        </select> <?php echo wc_help_tip( __( 'This lets you choose which products are part of this bundle.', JWBP_TEXT_DOMAIN ) ); // WPCS: XSS ok. ?>
    </p>
    <p class="form-field bundle_view_info_field">
        <label for="bundle_view_info"><?php _e('Bundle View Info', JWBP_TEXT_DOMAIN) ?></label>
        <textarea class="short" name="bundle_view_info" id="bundle_view_info"><?php echo '' ?><?php echo $bundle_view_info ?></textarea>
    </p>
    <p class="form-field bundle_short_description_field">
        <label for="bundle_short_description"><?php _e('Bundle Short Description', JWBP_TEXT_DOMAIN) ?></label>
        <textarea class="short" name="bundle_short_description" id="bundle_short_description"><?php echo $bundle_short_description ?></textarea>
    </p>
    <p class="form-field bundle_sale_price_field">
        <label for="bundle_sale_price"><?php _e('Bundle Sale Price', JWBP_TEXT_DOMAIN) ?></label>
        <input type="text" class="short" style="" name="bundle_sale_price" id="bundle_sale_price" value="<?php echo $bundle_sale_price ?>" placeholder="">
    </p>
</div>