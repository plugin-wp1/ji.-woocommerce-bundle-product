<?php

class Ji_Woocommerce_Bundle_Product_Admin_Product {

    public function __construct( ) {
		add_action( 'woocommerce_product_data_tabs', array($this,'jwbp_productTab') );
		/** Adding order preparation days */
		add_action( 'woocommerce_product_data_panels', array($this,'jwbp_order_preparation_days') );
		add_action( 'woocommerce_process_product_meta', array($this,'jwbp_order_preparation_days_save') );
    }

    function jwbp_productTab($tabs){
        $tabs['jwbp_bundle'] = array(
            'label'    => 'Ji. Bundle Product',
            'target'   => 'jwbp_bundle',
            'priority' => 20,
            'class' => 'hide_if_grouped hide_if_external'
        );
        return $tabs;
    }
    
    function jwbp_order_preparation_days() {
		global $product;

		$jwbp_data = get_post_meta($product->get_ID(), 'jwbp_bundle', true);

		echo '<div id="jwbp_bundle" class="panel woocommerce_options_panel hidden">';
		
		woocommerce_wp_checkbox( array(
            'label' => __("Enable Ji. Bundle Product", JWBP_TEXT_DOMAIN),
            'id' => 'jwbp_bundle_enable',
            'name' => 'jwbp_bundle_enable',
			'value' => $jwbp_data['status'],
            'description' => __("Enable Ji. Bundle Product for this product", JWBP_TEXT_DOMAIN)
        ));
         
		woocommerce_wp_text_input( array(
			'label' => __("Ji. Bundle IDs", JWBP_TEXT_DOMAIN),
			'id' => 'jwbp_bundle_ids',
			'name' => 'jwbp_bundle_ids',
			'type' => 'text',
			'value' => $jwbp_data['ids'],
			'description' => __("Enter some Ji. Bundle IDs by ',' separating values.", JWBP_TEXT_DOMAIN)
		));

		echo '</div>';
    }
    
    function jwbp_order_preparation_days_save( $post_id ) {
        $product = wc_get_product( $post_id );

		$value = array(
			'status' => isset($_POST['jwbp_bundle_enable']) ? 'yes' : 'no',
			'ids' => $_POST['jwbp_bundle_ids']
		);
        $product->update_meta_data( 'jwbp_bundle', $value );
        $product->save();
   }
    
}

new Ji_Woocommerce_Bundle_Product_Admin_Product();